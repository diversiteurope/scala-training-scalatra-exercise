package com.example.app

trait JsonSugar {

  import org.json4s._
  import org.json4s.jackson.Serialization

  implicit val formats = DefaultFormats

  implicit def stringToJson(s: String) = new {
    def as[T : Manifest]: T = Serialization.read[T](s)
  }

  def asJson[T <: AnyRef](t: T): String = Serialization.write(t)
}
