package com.example.app.datastore

import com.example.app.model.User
import org.scalatest.{Matchers, TryValues, WordSpec}

class JsonStoreTest extends WordSpec with Matchers with TryValues {

  import com.example.app.TestUsers._

  "JsonStore" should {

    "store to and read users from a file as json" in {

      val file = "target/test-users.json"
      val store = new JsonStore[User](file)
      store.storeInFile(users)

      val loadedUsers = store.loadFromFile

      loadedUsers.success.value should contain theSameElementsAs (users)
    }
  }
}

