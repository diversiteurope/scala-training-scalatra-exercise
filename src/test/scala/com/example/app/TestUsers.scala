package com.example.app

import com.example.app.model.User

object TestUsers {

  val user1 = User(1, "user1", 24, List("hiking", "biking"))

  val newUser = User(9, "user9", 99, List("being", "new"))

  val users = List(
    user1,
    User(2, "user2", 32, List("games")),
    User(3, "user3", 41, List("diving", "travelling", "baking"))
  )

}
