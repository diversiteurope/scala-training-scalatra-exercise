package com.example.app.service

import com.example.app.datastore.JsonStore
import com.example.app.model.User
import org.scalatest.{Matchers, OptionValues, TryValues, WordSpec}
import org.scalatest.mockito.MockitoSugar

import scala.util.Try

class UserServiceTest extends WordSpec with Matchers with MockitoSugar
    with TryValues with OptionValues {

  import org.mockito.Mockito._
  import com.example.app.TestUsers._

//  val userStore: JsonStore[User] = ???
//  val service: UserService = ???

  "UserService" should {

    "return all users" in {
      pending
    }

    "get a single user" in {
      pending
    }

    "return None for unknown user" in {
      pending
    }

    "store a user" in {
      pending
    }
  }
}
