package com.example.app.controller

import com.example.app.JsonSugar
import com.example.app.model.User
import com.example.app.service.UserService
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import org.scalatra.test.scalatest.ScalatraWordSpec

import scala.util.{Failure, Try}

class UserControllerTest extends ScalatraWordSpec with MockitoSugar with JsonSugar
    with BeforeAndAfterEach {

  import org.mockito.Mockito._
  import com.example.app.TestUsers._

  val mockService = mock[UserService]

  addServlet(new UserController(mockService), "/users/*")

  override def beforeEach = {
    reset(mockService)
  }

  "UsersController" when {

    "calling GET /users" when {

      "return a all users as json" in {
        pending
      }

      "return error when cannot retrieve users" in {
        pending
      }
    }

    "calling GET /users/:id" when {

      "return a single user" in {
        pending
      }

      "return error when cannot retrieve user" in {
        pending
      }
    }

    "calling POST /users" when {

      "store a new user" in {
        pending
      }

      "return error when cannot store user" in {
        pending
      }
    }
  }
}
