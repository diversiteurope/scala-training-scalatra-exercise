import com.example.app.controller.UserController
import com.example.app.datastore.JsonStore
import com.example.app.model.User
import com.example.app.{JsonSugar, model}
import com.example.app.service.UserService
import org.scalatra.test.scalatest.ScalatraWordSpec

class ScalatraBootstrapTest extends ScalatraWordSpec with JsonSugar {

  import com.example.app.TestUsers._

  addServlet(new UserController(new UserService(new JsonStore[model.User]("target/scala-2.12/classes/users.json"))), "/users")

  "Integration test" when {

    "create a users" should {
      "add a new user" in {

        // add user
        post("/users", asJson(newUser)) {
          pending
        }

        // verify it has been added
        get("/users") {
          pending
        }
      }
    }

    "getting users" should {
      "return all users" in {

        get("/users") {
          pending
        }
      }
    }

    "getting a single user" should {

      "return a known user" in {
        get("/users/1") {
          pending
        }
      }

      "return unknown" in {

        get("/users/99") {
          pending
        }
      }
    }

  }
}
