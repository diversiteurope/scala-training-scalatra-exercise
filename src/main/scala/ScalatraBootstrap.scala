import javax.servlet.ServletContext

import com.example.app._
import com.example.app.controller.UserController
import com.example.app.datastore.JsonStore
import com.example.app.service.UserService
import org.scalatra._

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    context.mount(new MyScalatraServlet, "/*")
    // TODO Add the UserController
  }
}
