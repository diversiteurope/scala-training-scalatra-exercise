package com.example.app.controller

import com.example.app.service.UserService
import org.json4s.DefaultFormats
import org.scalatra._
import org.scalatra.json._

class UserController(service: UserService) extends ScalatraServlet with JacksonJsonSupport {

  protected implicit lazy val jsonFormats = DefaultFormats


  before() {
    contentType = formats("json")
  }

  get("/") {
    // TODO return all users
  }

  // TODO add addional endpoints for:
  // - get a single user
  // - create a new user

  // (additional)
  // - update a user
  // - find one (or multiple) users
  // - delete a user

  // (more additional)
  // instead of returning json,
  // return a webpage using templates

}
