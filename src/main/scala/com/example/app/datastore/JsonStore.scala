package com.example.app.datastore

import java.io.PrintWriter

import scala.io.Source
import scala.util.Try

/**
  * Storing and loading items in a file as json.
  *
  * @param filename Filename in which to store data as json
  * @tparam T Type of items to store and load.
  */
class JsonStore[T](filename: String) {
  import org.json4s._
  import org.json4s.jackson.Serialization

  implicit val formats = DefaultFormats

  // create file if does not exist
  if (!new java.io.File(filename).exists()) {
    println(s"Creating file: $filename")
    new java.io.File(filename).createNewFile()
  }

  /**
    * @return List of items from file.
    */
  def loadFromFile(implicit mfl:Manifest[List[T]]): Try[List[T]] = Try {

    val data = Source.fromFile(filename).mkString
    val items = Serialization.read[List[T]](data)
    items
  }

  /**
    * @param data List of items to store as json in a file.
    */
  def storeInFile(data: List[T]): Try[Unit] = Try {

    val json = Serialization.write(data)

    val writer = new PrintWriter(filename)
    writer.println(json)
    writer.flush()
    writer.close()
  }
}