package com.example.app.service

import com.example.app.datastore.JsonStore
import com.example.app.model.{Identifiable, User}

import scala.util.Try

/**
  * Generic service for handling entity `T`.
  * @tparam T Entity type
  */
trait Service[T <: Identifiable] {
  val store: JsonStore[T]

  // add the methods you need
}

/**
  * User service.
  * @param store A [[JsonStore]] to store and retrieve users.
  */
class UserService(val store: JsonStore[User]) extends Service[User] {

}


