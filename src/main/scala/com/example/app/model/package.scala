package com.example.app

package object model {

  /** Marks an identifiable entity */
  trait Identifiable {
    val id: Int
  }

  /** The user entity */
  case class User(id: Int, name: String, age: Int, hobbies: List[String]) extends Identifiable
}
